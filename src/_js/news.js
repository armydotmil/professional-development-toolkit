// Last Modified 2016-01-29 FKM
$.getJSON( "http://www.army.mil/api/packages/getpackagesbykeywords?keywords=target_profdev%2Ctarget_pdf&count=25&limit=1", function( data ) {
    var news_items = '<div class="widgetWrap"><h2 class="micrositeStoryListMoreWidget">STORIES</h2>';

    $.each( data, function( key, val ) { 
        news_items = news_items + '<div class="storyBlock micrositeStoryListMoreWidget"><div class="leftPane"><a href="'+val.page_url+'"><img alt="http://www.army.mil'+val.images[0].alt+'" src="http://www.army.mil'+val.images[0].url_size3+'" ></img></a></div><div class="rightPane"><h3><a href="'+val.page_url+'">'+ellipsis(val.title, true)+'</a></h3><p style="height: 70px;">'+ellipsis(val.description, false)+'<a href="'+val.page_url+'" >&nbsp;MORE<span class="access"></span></a></p></div></div>';       
    });

    news_items = news_items + '<div id="archives"><a href="javascript:;" title="More Stories">More stories</a></div></div>';

    $(document).ready(function() {
        
        $("#micrositeLeftCol").append(news_items);
        var display = 5;
        var storyLength = $(".storyBlock").length;
        $(".storyBlock").hide();
        $(".storyBlock:lt(" + display + ")").show();
        $("#archives").click(function() {
            if (display < storyLength) {
                display = display + 10;
                $(".storyBlock:lt(" + display + ")").show();
            } else {
                $("#archives").hide();
            }
            return false;
        });
    });

});

function ellipsis(newsString, isTitle) { 
   
    var maxStringLength;
 
    if(isTitle) {// titles
        maxStringLength = 46;
        if(newsString.length > maxStringLength) {
            newsString = newsString.substring(0, maxStringLength);
            newsString += "...";
            return newsString;
        }
        else 
            return newsString;
    }
    else {// descriptions
        maxStringLength = 210;
        if(newsString.length > maxStringLength) {
            newsString = newsString.substring(0, maxStringLength);
            newsString += "...";
            return newsString;
        }
        else
            return newsString;
    } 
    
}



   

    
    
 



